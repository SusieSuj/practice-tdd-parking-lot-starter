package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingBoyTest {
    @Test
    void should_return_a_parking_ticket_when_parkedByParkingBoy_given_a_car(){
        ParkingBoy parkingBoy = new ParkingBoy();
        Car car = new Car();
        ParkinglotTicket parkinglotTicket = parkingBoy.park(car);
        assertNotNull(parkinglotTicket);
    }

    @Test
    void should_return_a_parked_car_when_fetchedByParkingBoy_given_a_ticket(){
        ParkingBoy parkingBoy = new ParkingBoy();
        Car car = new Car();
        ParkinglotTicket parkinglotTicket = parkingBoy.park(car);
        Car fetchedCar = parkingBoy.fetch(parkinglotTicket);
        assertEquals(car,fetchedCar);
    }

    @Test
    void should_return_the_right_cars_when_fetchedByParkingBoy_given_two_tickets(){
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingBoy parkingBoy = new ParkingBoy();
        ParkinglotTicket parkinglotTicket1 = parkingBoy.park(car1);
        ParkinglotTicket parkinglotTicket2 = parkingBoy.park(car2);
        assertEquals(car1,parkingBoy.fetch(parkinglotTicket1));
        assertEquals(car2,parkingBoy.fetch(parkinglotTicket2));

    }

    @Test
    void should_return_message_when_fetchedByParkingBoy_given_wrong_parking_ticket(){
        Car car = new Car();
        ParkingBoy parkingBoy = new ParkingBoy();
        ParkinglotTicket parkinglotTicket = parkingBoy.park(car);
        ParkinglotTicket parkinglotTicketWrong = new ParkinglotTicket();
        assertNotEquals(parkinglotTicket,parkinglotTicketWrong);
        RuntimeException exception = assertThrows(RuntimeException.class, () -> parkingBoy.fetch(new ParkinglotTicket()));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_message_when_fetchedByParkingBoy_given_a_used_parking_ticket(){
        Car car = new Car();
        ParkingBoy parkingBoy = new ParkingBoy();
        ParkinglotTicket parkinglotTicket = parkingBoy.park(car);

        Car fetchCar1 = parkingBoy.fetch(parkinglotTicket);
        RuntimeException exception = assertThrows(RuntimeException.class, () -> parkingBoy.fetch(new ParkinglotTicket()));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_messsage_when_parklot_has_no_position(){
        ParkingBoy parkingBoy = new ParkingBoy();
        for (int i = 0; i < 10; i++) {
            parkingBoy.park(new Car());
        }
        RuntimeException exception = assertThrows(RuntimeException.class, () -> parkingBoy.park(new Car()));
        assertEquals("no availiable position.", exception.getMessage());
    }

    @Test
    void should_return_ticket_in_the_second_parkinglot_when_parkedByBoy_given_cars_and_the_first_parkinglot_has_position(){
        ParkingBoy parkingBoy = new ParkingBoy(2);
        for (int i = 0; i < 5; i++) {
            parkingBoy.park(new Car());
        }
        ParkinglotTicket parkinglotTicket = parkingBoy.park(new Car());
        assertNotNull(parkinglotTicket);
        assertEquals(1,parkinglotTicket.getParkinglotNumber());

    }

    @Test
    void should_return_ticket_in_the_second_parkinglot_when_parkedByBoy_given_cars_and_the_first_parkinglot_full(){
        ParkingBoy parkingBoy = new ParkingBoy(2);
        for (int i = 0; i < 10; i++) {
            parkingBoy.park(new Car());
        }
        ParkinglotTicket parkinglotTicket = parkingBoy.park(new Car());
        assertNotNull(parkinglotTicket);
        assertEquals(2,parkinglotTicket.getParkinglotNumber());

    }

    @Test
    void should_return_right_car_when_parkingBoy_given_two_tickets(){
        ParkingBoy parkingBoy = new ParkingBoy(2);
        Car car1 = null;
        ParkinglotTicket parkinglotTicket1 = null;
        for (int i = 0; i < 10; i++) {
            if (i==5){
                 car1 = new Car();
                 parkinglotTicket1 = parkingBoy.park(car1);
            }
            parkingBoy.park(new Car());
        }
        Car car2 = new Car();
        ParkinglotTicket parkinglotTicket2 = parkingBoy.park(car2);
        Car fetchCar1 = parkingBoy.fetch(parkinglotTicket1);
        Car fetchCar2 = parkingBoy.fetch(parkinglotTicket2);
        assertEquals(car1,fetchCar1);
        assertEquals(car2,fetchCar2);
    }

    @Test
    void should_return_message_when_parkingBoy_given_the_wrong_ticket(){
        ParkingBoy parkingBoy = new ParkingBoy(2);
        parkingBoy.park(new Car());
        RuntimeException exception = assertThrows(RuntimeException.class, () -> parkingBoy.fetch(new ParkinglotTicket()));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_message_when_parkingBoy_given_the_used_ticket(){
        Car car = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(2);
        ParkinglotTicket parkinglotTicket = parkingBoy.park(car);

        Car fetchCar1 = parkingBoy.fetch(parkinglotTicket);
        RuntimeException exception = assertThrows(RuntimeException.class, () -> parkingBoy.fetch(new ParkinglotTicket()));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_messsage_when_parklots_has_no_position(){
        ParkingBoy parkingBoy = new ParkingBoy(2);
        for (int i = 0; i < 20; i++) {
            parkingBoy.park(new Car());
        }
        RuntimeException exception = assertThrows(RuntimeException.class, () -> parkingBoy.park(new Car()));
        assertEquals("no availiable position.", exception.getMessage());
    }
}
