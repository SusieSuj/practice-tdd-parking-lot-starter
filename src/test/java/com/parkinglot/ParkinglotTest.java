package com.parkinglot;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkinglotTest {
    @Test
    void should_return_a_ticket_when_park_given_a_car() {
        Car car = new Car();
        Parkinglot parkinglot = new Parkinglot(10);
        ParkinglotTicket paringlotTicket = parkinglot.park(car);
        assertNotNull(paringlotTicket);
    }


    @Test
    void should_return_a_parked_car_when_fetch_given_a_parked_car_and_parking_ticket() {
        Car car = new Car();
        Parkinglot parkinglot = new Parkinglot(10);
        ParkinglotTicket parkinglotTicket = parkinglot.park(car);
        Car parkedCar = parkinglot.fetch(parkinglotTicket);
        assertEquals(car, parkedCar);

    }


    @Test
    void should_return_different_tickets_when_park_given_two_cars() {
        Car car1 = new Car();
        Car car2 = new Car();
        Parkinglot parkinglot = new Parkinglot(10);
        ParkinglotTicket parkinglotTicket1 = parkinglot.park(car1);
        ParkinglotTicket parkinglotTicket2 = parkinglot.park(car2);
        assertNotSame(parkinglotTicket1, parkinglotTicket2);
    }

    @Test
    void should_return_exception_when_fetch_given_a_wrong_ticket() {
        Car car = new Car();
        Parkinglot parkinglot = new Parkinglot(10);
        ParkinglotTicket parkinglotTicket = parkinglot.park(car);
        RuntimeException exception = assertThrows(RuntimeException.class, () -> parkinglot.fetch(new ParkinglotTicket()));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_return_null_when_fetch_given_a_used_ticket() {
        Car car = new Car();
        Parkinglot parkinglot = new Parkinglot(10);
        ParkinglotTicket parkinglotTicket = parkinglot.park(car);

        Car fetchCar1 = parkinglot.fetch(parkinglotTicket);
        RuntimeException exception = assertThrows(RuntimeException.class, () -> parkinglot.fetch(new ParkinglotTicket()));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_null_when_parklot_has_no_position() {
        Parkinglot parkinglot = new Parkinglot(10);
        for (int i = 0; i < 10; i++) {
            parkinglot.park(new Car());
        }
        RuntimeException exception = assertThrows(RuntimeException.class, () -> parkinglot.park(new Car()));
        assertEquals("no availiable position.", exception.getMessage());
    }


}
