package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SuperSmartParkingBoyTest {

    @Test
    void should_return_a_ticket_when_park_given_a_car(){
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(2);
        Car car = new Car();
        ParkinglotTicket parkinglotTicket = superSmartParkingBoy.park(car);
        assertNotNull(parkinglotTicket);
    }

    @Test
    void should_return_different_tickets_when_park_given_2_cars(){
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(2);
        ParkinglotTicket parkinglotTicket1 = superSmartParkingBoy.park(new Car());
        ParkinglotTicket parkinglotTicket2 = superSmartParkingBoy.park(new Car());
        assertEquals(1,parkinglotTicket1.getParkinglotNumber());
        assertEquals(2,parkinglotTicket2.getParkinglotNumber());
//        assertNotEquals(parkinglotTicket1,parkinglotTicket2);
    }

    @Test
    void should_return_a_car_when_fetch_given_a_ticket(){
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(2);
        Car car = new Car();
        ParkinglotTicket parkinglotTicket = superSmartParkingBoy.park(car);
        Car fetchCar = superSmartParkingBoy.fetch(parkinglotTicket);
        assertEquals(car,fetchCar);
    }
    @Test
    void should_return_right_cars_when_fetch_given_2_cars() {
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(2);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkinglotTicket parkinglotTicket1 = superSmartParkingBoy.park(car1);
        ParkinglotTicket parkinglotTicket2 = superSmartParkingBoy.park(car2);
        Car fetchCar1 = superSmartParkingBoy.fetch(parkinglotTicket1);
        Car fetchCar2 = superSmartParkingBoy.fetch(parkinglotTicket2);
        assertEquals(car1, fetchCar1);
        assertEquals(car2, fetchCar2);
    }
    @Test
    void should_return_message_when_fetch_given_wrong_ticket() {
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(2);
        superSmartParkingBoy.park(new Car());
        RuntimeException exception = assertThrows(RuntimeException.class, () -> superSmartParkingBoy.fetch(new ParkinglotTicket()));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_rerurn_message_when_fetch_given_a_used_ticket() {
        Car car = new Car();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(2);
        ParkinglotTicket parkinglotTicket = superSmartParkingBoy.park(car);

        Car fetchCar1 = superSmartParkingBoy.fetch(parkinglotTicket);
        RuntimeException exception = assertThrows(RuntimeException.class, () -> superSmartParkingBoy.fetch(new ParkinglotTicket()));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_messsage_when_park_parkinglot_has_no_position(){
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(2);
        for (int i = 0; i < 20; i++) {
            superSmartParkingBoy.park(new Car());
        }
        RuntimeException exception = assertThrows(RuntimeException.class, () -> superSmartParkingBoy.park(new Car()));
        assertEquals("no availiable position.", exception.getMessage());
    }
}
