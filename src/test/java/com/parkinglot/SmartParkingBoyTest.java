package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SmartParkingBoyTest {
    @Test
    void should_return_parkingTicket_in_first_parkinglot_when_park_given_2_empty_parkinglot_and_a_car() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(2);
        ParkinglotTicket parkinglotTicket = smartParkingBoy.park(new Car());
        assertNotNull(parkinglotTicket);
        assertEquals(1, parkinglotTicket.getParkinglotNumber());
    }

    @Test
    void should_return_parkingTicket_and_different_parkinglotNumber_when_park_given_2_car() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(2);
        ParkinglotTicket parkinglotTicket1 = smartParkingBoy.park(new Car());
        ParkinglotTicket parkinglotTicket2 = smartParkingBoy.park(new Car());
        assertEquals(1, parkinglotTicket1.getParkinglotNumber());
        assertEquals(2, parkinglotTicket2.getParkinglotNumber());
    }

    @Test
    void should_return_a_parking_car_when_fetch_given_a_ticket() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(2);
        Car car = new Car();
        ParkinglotTicket parkinglotTicket = smartParkingBoy.park(car);
        Car fetchCar = smartParkingBoy.fetch(parkinglotTicket);
        assertEquals(car, fetchCar);
    }


    @Test
    void should_return_right_cars_when_fetch_given_2_cars() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(2);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkinglotTicket parkinglotTicket1 = smartParkingBoy.park(car1);
        ParkinglotTicket parkinglotTicket2 = smartParkingBoy.park(car2);
        Car fetchCar1 = smartParkingBoy.fetch(parkinglotTicket1);
        Car fetchCar2 = smartParkingBoy.fetch(parkinglotTicket2);
        assertEquals(car1, fetchCar1);
        assertEquals(car2, fetchCar2);
    }

    //error message
    @Test
    void should_return_message_when_fetch_given_wrong_ticket() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(2);
        smartParkingBoy.park(new Car());
        RuntimeException exception = assertThrows(RuntimeException.class, () -> smartParkingBoy.fetch(new ParkinglotTicket()));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_rerurn_message_when_fetch_given_a_used_ticket() {
        Car car = new Car();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(2);
        ParkinglotTicket parkinglotTicket = smartParkingBoy.park(car);

        Car fetchCar1 = smartParkingBoy.fetch(parkinglotTicket);
        RuntimeException exception = assertThrows(RuntimeException.class, () -> smartParkingBoy.fetch(new ParkinglotTicket()));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }


}
