package com.parkinglot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ParkingBoy {
    private final List<Parkinglot> parkinglots = new ArrayList<>();

    public ParkingBoy() {
        parkinglots.add(new Parkinglot(10));
        parkinglots.get(0).setParkinglotNumber(1);

    }

    public ParkingBoy(int parkinglotQuantity) {
        for (int i = 0; i < parkinglotQuantity; i++) {
            parkinglots.add(new Parkinglot(10));
            parkinglots.get(i).setParkinglotNumber(i+1);
        }
    }

    public ParkinglotTicket park(Car car) {
        for (Parkinglot parkinglot : parkinglots) {
            if (parkinglot.ifFull()) {
                System.out.println(parkinglot.ifFull());
                continue;
            }
            return parkinglot.park(car);
        }
        throw new RuntimeException("no availiable position.");
    }

    public Car fetch(ParkinglotTicket parkinglotTicket) {
        for (Parkinglot parkinglot : parkinglots) {
            if (parkinglot.ifHaveRightCar(parkinglotTicket))
                return parkinglot.fetch(parkinglotTicket);
        }
        throw new RuntimeException("Unrecognized parking ticket.");
    }
}
