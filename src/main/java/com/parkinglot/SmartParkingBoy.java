package com.parkinglot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SmartParkingBoy {
    private final List<Parkinglot> parkinglots = new ArrayList<>();

    public SmartParkingBoy() {
        parkinglots.add(new Parkinglot(10));
        parkinglots.get(0).setParkinglotNumber(1);
    }

    public SmartParkingBoy(int parkinglotQuantity) {
        for (int i = 0; i < parkinglotQuantity; i++) {
            parkinglots.add(new Parkinglot(10));
            parkinglots.get(i).setParkinglotNumber(i+1);
        }
    }

    public ParkinglotTicket park(Car car) {
        return parkinglots.stream()
                .max(Comparator.comparing(Parkinglot::getAvailablePosition))
                .stream().findFirst()
                .orElseThrow(()->new RuntimeException("no availiable position."))
                .park(car);
    }

    public Car fetch(ParkinglotTicket parkinglotTicket) {
        for (Parkinglot parkinglot : parkinglots) {
            if (parkinglot.ifHaveRightCar(parkinglotTicket))
                return parkinglot.fetch(parkinglotTicket);
        }
        throw new RuntimeException("Unrecognized parking ticket.");
    }
}
