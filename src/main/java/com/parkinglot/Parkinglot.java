package com.parkinglot;


import javax.swing.plaf.PanelUI;
import java.util.HashMap;
import java.util.Map;

public class Parkinglot {

    private int capacity;
    private int availablePosition;
    private int parkinglotNumber;
    private double emptyRate;

    public Parkinglot() {
    }

    public Parkinglot(int capacity) {
        this.capacity = capacity;
        this.availablePosition = capacity;
        this.emptyRate = (double) this.availablePosition / this.capacity;
    }

    private final Map<ParkinglotTicket, Car> carsMap = new HashMap<>();

    public void setParkinglotNumber(int parkinglotNumber) {
        this.parkinglotNumber = parkinglotNumber;
    }

    public ParkinglotTicket park(Car car) {
        if (ifFull()) {
            throw new RuntimeException("no availiable position.");
        }

        ParkinglotTicket parkinglotTicket = new ParkinglotTicket();
        parkinglotTicket.setParkinglotNumber(parkinglotNumber);
        carsMap.put(parkinglotTicket, car);
        availablePosition--;
        updateEmptyRate();
        return parkinglotTicket;
    }

    private void updateEmptyRate() {
        this.emptyRate = (double) this.availablePosition / this.capacity;
    }

    public Car fetch(ParkinglotTicket parkinglotTicket) {
        Car car = null;
        if (isValidTicket(parkinglotTicket)) {
            throw new RuntimeException("Unrecognized parking ticket.");
        }

        if (carsMap.containsKey(parkinglotTicket)) {
            car = carsMap.get(parkinglotTicket);
            carsMap.remove(parkinglotTicket, car);
            availablePosition++;
            updateEmptyRate();
            parkinglotTicket.setValid(false);
        }
        return car;
    }

    public boolean ifFull() {
        if (1 > availablePosition) {
            System.out.println("availablePosition:" + availablePosition);
            return true;
        }
        return false;
    }

    public boolean isValidTicket(ParkinglotTicket parkinglotTicket) {
        return !(parkinglotTicket.getValid() && carsMap.containsKey(parkinglotTicket));
    }

    public boolean ifHaveRightCar(ParkinglotTicket parkinglotTicket) {
        return carsMap.containsKey(parkinglotTicket);
    }

    public int getAvailablePosition() {
        return availablePosition;
    }

    public double getEmptyRate() {
        return emptyRate;
    }

}
