package com.parkinglot.exception;

public class FetchCarException extends RuntimeException{
    public FetchCarException(String message) {
        super(message);
    }
}
