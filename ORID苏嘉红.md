# O

- In the morning, we had code review. I refactor my code according to my crew's suggestion. I learned another way to change every char in a String to a List<String>. The second method is to use the "Stream.of()",which is more concise than the method I first write.

- In the afternoon, our group members discussed the assignment of tasks for the weekend. Then we spend most of the time on coding. We write a program about the parking lot. We still use the TDD thoughts to coding. One of the benefits is that we think and solve problems step by step.

- Finally, we summerize what we have learned this week.

# R

- I feel fulfilled.

# I

- In the process of code review, I always learn new things from others. And I will try to refactor my code, which really benefit me a lot.

# D

- ​	Every time I finish writing code, I need to try to refactor it.

